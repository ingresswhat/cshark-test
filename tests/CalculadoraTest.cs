using Xunit;
using library;

namespace tests;


public class CalculadoraTest
{
    [Fact]
    public void Suma()
    {
        Calculadora cal = new Calculadora();
        int result = 0;

        result = cal.Suma(3,4);

        Assert.Equal(7, result);
    }
    [Fact]
    public void Resta()
    {
        Calculadora cal = new Calculadora();
        int result = 0;

        result = cal.Resta(3,4);

        Assert.Equal(-1, result);
    }
    [Fact]
    public void Multiplicacion()
    {
        Calculadora cal = new Calculadora();
        int result = 0;

        result = cal.Multiplicacion(3,4);

        Assert.Equal(12, result);
    }
    [Fact]
    public void Division()
    {
        Calculadora cal = new Calculadora();
        int result = 0;

        result = cal.Division(8,4);

        Assert.Equal(2, result);
    }
    [Fact]
    public void Division_byCero()
    {
        Calculadora cal = new Calculadora();
        int result = 0;

        result = cal.Division(8,0);

        Assert.Equal(0, result);
    }
    [Fact]
    public void PeersArray()
    {
        Calculadora cal = new Calculadora();
        int[] result = cal.GetPairNumbers(100);
        Assert.DoesNotContain(3, result);
    }
    [Theory]
    [InlineData(4)]
    [InlineData(8)]
    [InlineData(100)]
    [InlineData(240)]
    [InlineData(500)]
    [InlineData(104)]
    [InlineData(900)]
    public void PeersArray_NoContainsOdd(int number)
    {
        Calculadora cal = new Calculadora();
        int[] result = cal.GetPairNumbers(1000);
        Assert.Contains(number, result);
    }
}