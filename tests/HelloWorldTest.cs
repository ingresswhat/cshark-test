using Xunit;
using library;

namespace tests;


public class HelloWorldTest
{
    readonly HelloWorld hello;
    public HelloWorldTest()
    {
        hello = new HelloWorld();
    }

    [Fact]
    public void GetMessageWorld()
    {
        string result = string.Empty;

        result = hello.GetMessage();

        Assert.NotEmpty(result);
        Assert.NotEmpty(result);
        Assert.Equal("Hello World!", result);    
        Assert.Contains("Hello World!", result);    
    }
    
    [Fact]
    public void GetMessageWorld_Ends()
    {
        string result = string.Empty;

        result = hello.GetMessage();

        Assert.NotEmpty(result);
        Assert.NotEmpty(result);
        Assert.EndsWith("World!", result);    
    }
}