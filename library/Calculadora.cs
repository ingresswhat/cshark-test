﻿using System.Collections.Generic;

namespace library;
public class Calculadora
{
    public int Suma(int numberA, int numberB){
        return numberA + numberB;
    }
    public int Resta(int numberA, int numberB){
        return numberA - numberB;
    }
    public int Multiplicacion(int numberA, int numberB){
        return numberA * numberB;
    }
    public int Division(int numberA, int numberB){
        if(numberB == 0) return 0;

        return numberA/numberB;
    }
    public int[] GetPairNumbers(int limit)
    {
        List<int> result = new List<int>();
        for (int i = 1; i <= limit; i++)
        {
            if(i%2 == 0) result.Add(i);
        }
        return result.ToArray();
    }
}
